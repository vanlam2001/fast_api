from fastapi import FastAPI, Path, HTTPException, Depends
from typing import Optional
from pydantic import BaseModel
from pymongo import MongoClient
from passlib.context import CryptContext
from typing import Optional

app = FastAPI()

client = MongoClient("mongodb+srv://vanlam1412:5DFv9zNIh4bQaYBY@cluster0.8eouv1s.mongodb.net")
db = client["register"]
users_collection = db["users"]

class UserCreate(BaseModel):
    username: str
    password: str

class UserLogin(BaseModel):
    username: str
    password: str


# Mã hóa mật khẩu 
class HashedPassword:
    def __init__(self, hashed_password: str):
        self.hashed_password = hashed_password

class UserInDB(UserCreate, HashedPassword):
    pass 

password_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

def verify_password(plain_password, hashed_password):
    return password_context.verify(plain_password, hashed_password)

def get_password_hash(password):
    return password_context.hash(password)
                                
                                

students = {
    1: {
        "name": "john",
        "age": 17,
        "year": "year 12"
    },
    8: {
        "name": "zxzxzx",
        "age": 67,
        "year": "year 67"
    }
}

class Student(BaseModel):
    name: str
    age: int
    year: str

class UpdateStudent(BaseModel):
    name: Optional[str] = None
    age: Optional[int] = None
    year: Optional[str] = None

@app.get("/data")
def index():
    return {"name": "First Data"}

@app.get("/get-student/{student_id}")
def get_student(student_id: int = Path(..., description="The ID of the student you want to view", gt=0)):
    return students.get(student_id, {"Error": "Student not found"})

@app.get("/get-by-name/")
def get_student_by_name(name: str, test: int):
    result = [student for student_id, student in students.items() if student["name"] == name]
    if result:
        return result
    return {"Data": "Not Found"}

@app.post("/create-student/{student_id}")
def create_student(student_id: int, student: Student):
    if student_id in students:
        return {"Error": "Student exists"}
    
    students[student_id] = student.dict()
    return students[student_id]

@app.put("/update-student/{student_id}")
def update_student(student_id: int, student: UpdateStudent):
    if student_id not in students:
        return {"Error": "Student does not exist"}
    
    # Update only the provided fields
    for field, value in student.dict().items():
        if value is not None:
            students[student_id][field] = value
    
    return students[student_id]

@app.delete("/delete-student/{student_id}")
def delete_student(student_id: int):
    if student_id not in students:
        return {"Error": "Student dose not exist"}
    del students[student_id]
    return {"Message": "Student deleted successfully"}

@app.post("/register/")
async def register(user: UserCreate):
    hashed_password = get_password_hash(user.password)
    
    # Kiểm tra xem tên người dùng đã tồn tại chưa
    if users_collection.find_one({"username": user.username}):
        raise HTTPException(status_code=400, detail="Username already exists")
    
    # Thêm người dùng vào MongoDB
    user_data = {"username": user.username, "password": hashed_password}
    users_collection.insert_one(user_data)
    
    return {"message": "User registered successfully"}

@app.post("/login")
async def login(user: UserLogin):
    stored_user = users_collection.find_one({"username": user.username})
    if stored_user and verify_password(user.password, stored_user["password"]):
        return {"message": "Login successful"}
    raise HTTPException(status_code=401, detail="Incorrect username or password")

@app.delete("/delete/{username}")
async def delete_account(username: str):
    result = users_collection.delete_one({"username": username})
    if result.deleted_count == 1:
        return {"message": "User account deleted successfully"}
    raise HTTPException(status_code=404, detail="User not found") 







    
